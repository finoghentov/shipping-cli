<?php

namespace Pavel\ShippingCli\Commands;

use Pavelf\Container\Calculator;
use Pavelf\Container\Container;
use Pavelf\Container\ContainerType;
use Pavelf\Container\Package;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

#[AsCommand(
    name: 'shipping:calculate',
    description: 'Run calculator application'
)]
class Calculate extends Command
{
    protected array $containerTypes = [];

    protected array $packageTypes = [];

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Please, enter data that you want calculate.');
        $output->writeln([
            '',
            'Available container types.'
        ]);

        $this->handleContainerTypes($input, $output);

        $output->writeln([
            '',
            'Available package types.'
        ]);

        $this->handlePackageTypes($input, $output);


        $result = $this->calculate();

        $this->printResult($result, $output);

        return 0;
    }

    protected function handleContainerTypes(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');
        $confirmation = true;

        while ($confirmation) {
            $width = $helper->ask($input, $output, new Question('Width: '));
            $length = $helper->ask($input, $output, new Question('Length: '));
            $height = $helper->ask($input, $output, new Question('Height: '));

            if (!$this->validateDimensions([$width, $length, $height])) {
                $output->writeln('You entered incorrect dimensions. Please repeat.');
                continue;
            }

            if (isset($this->containerTypes[$width.$length.$height])) {
                $output->writeln('Duplicated container. Skipping...');
                continue;
            }

            $this->containerTypes[$width.$length.$height] = new ContainerType($width, $length, $height);

            if (!$helper->ask($input, $output, new ConfirmationQuestion('You want to add more containers? [yes]  '))) {
                $confirmation = false;
            }
        }
    }

    protected function handlePackageTypes(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');
        $confirmation = true;

        while ($confirmation) {
            $width = $helper->ask($input, $output, new Question('Width: '));
            $length = $helper->ask($input, $output, new Question('Length: '));
            $height = $helper->ask($input, $output, new Question('Height: '));
            $amount = $helper->ask($input, $output, new Question('Amount of packages: '));

            if (!$this->validateDimensions([$width, $length, $height, $amount])) {
                $output->writeln('You entered incorrect dimensions. Please repeat.');
                continue;
            }

            if (isset($this->packageTypes[$width.$length.$height])) {
                $output->writeln('Duplicated container. Skipping...');
                continue;
            }

            $this->packageTypes[$width.$length.$height] = [$width, $length, $height, $amount];

            if (!$helper->ask($input, $output, new ConfirmationQuestion('You want to add more packages? [yes]  '))) {
                $confirmation = false;
            }
        }
    }

    /**
     * @return array<Container>
     */
    protected function calculate(): array
    {
        $calc = new Calculator();

        foreach ($this->packageTypes as $packageType) {
            [$width, $length, $height, $amount] = $packageType;

            for ($i = 0; $i < $amount; $i++) {
                $calc->pack(new Package($width, $length, $height));
            }
        }

        foreach ($this->containerTypes as $containerType) {
            $calc->to($containerType);
        }

        return $calc->process();
    }

    /**
     * @param array<Container> $containers
     * @param array $testCase
     * @return void
     */
    protected function printResult(array $containers, OutputInterface $output): void
    {
        $output->writeln(['', 'Following packages:']);

        foreach ($containers as $container) {
            $innerPackages = [];

            foreach ($container->getPackages() as $package) {
                $key = $package->width . 'x' . $package->length . 'x' . $package->height;

                $innerPackages[$key] = ($innerPackages[$key] ?? 0) + 1;
            }

            $output->writeln("Container with next dimensions: width={$container->width}, length={$container->length}, height=$container->height. Can hold:");
            foreach ($innerPackages as $key => $amount) {
                $output->writeln("$amount items of $key");
            }
        }
        $output->writeln(sprintf("Totally we need at least [%s] container(s)", count($containers)));
    }

    protected function validateDimensions(array $dimensions): bool
    {
        foreach ($dimensions as $dimension) {
            if (!is_numeric($dimension) || $dimension <= 0) {
                return false;
            }
        }

        return true;
    }
}