#!/bin/bash

if [[ "$(docker images -q shipping-cli 2> /dev/null)" == "" ]]; then
  docker build -t shipping-cli .
fi

docker run -it --rm shipping-cli php index.php shipping:calculate