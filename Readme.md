# Shipping CLI application

## What this app can do?
This app is just a convenient way to interact with [Shipping Calculator](https://gitlab.com/finoghentov/shipping-calculator).

## Requirments
- Docker

## How to run

```bash
./run.sh
```