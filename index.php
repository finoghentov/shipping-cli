<?php

use Pavel\ShippingCli\Commands\Calculate;
use Symfony\Component\Console\Application;

require 'vendor/autoload.php';

if (php_sapi_name() !== 'cli') {
    die();
}

$application = new Application();

$application->add(new Calculate());

$application->run();