FROM php:8.2-cli

RUN apt update && \
    apt install -y \
    git \
    libzip-dev \
    zip  \
    unzip  \
    libicu-dev  \
    procps  \
    libxml2-dev  \
    curl  \
    libcurl4-openssl-dev

RUN docker-php-ext-install dom intl curl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

COPY . /app

WORKDIR /app

RUN composer install